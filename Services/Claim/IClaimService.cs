namespace Services.Claim;
using Data.Models;

public interface IClaimService
{
    Task<IEnumerable<Claim>> GetClaimsAsync();
    Task<Claim?> GetClaimByIdAsync(string id);
    Task AddClaimAsync(Claim dataItem);
    Task DeleteClaimAsync(string id);
}