namespace Services.Claim;
using Repository.Cosmos;
using Data.Models;

public class ClaimService : IClaimService
{
    private readonly ICosmosRepository<Claim> _cosmosRepository;
    
    public ClaimService(ICosmosRepository<Claim> cosmosRepository)
    {
        _cosmosRepository = cosmosRepository;
    }

    public async Task<IEnumerable<Claim>> GetClaimsAsync()
    {
        return await _cosmosRepository.GetItemsAsync();
    }

    public async Task<Claim?> GetClaimByIdAsync(string id)
    {
        return await _cosmosRepository.GetItemByIdAsync(id);
    }

    public Task AddClaimAsync(Claim dataItem)
    {
        return _cosmosRepository.AddItemAsync(dataItem);
    }

    public Task DeleteClaimAsync(string id)
    {
        return _cosmosRepository.DeleteItemAsync(id);
    }
}