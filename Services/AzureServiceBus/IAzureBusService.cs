using Data.Models;

namespace Services.AzureServiceBus;
    
public interface IAzureBusService
{
    Task SendMessageAsync(BaseAudit message);
}