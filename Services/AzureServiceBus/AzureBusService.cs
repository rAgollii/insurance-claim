using Microsoft.Azure.ServiceBus;
using System.Text;
using Data.Models;
using Newtonsoft.Json;

namespace Services.AzureServiceBus;

public class AzureBusService : IAzureBusService
{
    private readonly IQueueClient _queueClient;

    public AzureBusService(string? connectionString, string? queueName)
    {
        if (string.IsNullOrEmpty(connectionString))
        {
            throw new ArgumentNullException(nameof(connectionString), "Connection string is required.");
        }
        if (string.IsNullOrEmpty(queueName))
        {
            throw new ArgumentNullException(nameof(queueName), "Queue name is required.");
        }
        _queueClient = new QueueClient(connectionString, queueName);
    }

    public async Task SendMessageAsync(BaseAudit message)
    {
        var jsonMessage = JsonConvert.SerializeObject(message);
        var encodedMessage = new Message(Encoding.UTF8.GetBytes(jsonMessage));
        await _queueClient.SendAsync(encodedMessage);
    }
}