namespace Services.Cover;

using Data.Enums;
using Data.Models;

public interface ICoverService
{
    decimal ComputePremium(DateOnly startDate, DateOnly endDate, CoverType coverType);
    Task<IEnumerable<Cover>> GetCoversAsync();
    Task<Cover?> GetCoverByIdAsync(string id);
    Task AddCoverAsync(Cover cover);
    Task DeleteCoverAsync(string id);
}