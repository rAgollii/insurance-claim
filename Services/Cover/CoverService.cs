using Data.Enums;
using Repository.Cosmos;

namespace Services.Cover;
using Data.Models;

public class CoverService : ICoverService
{
    private readonly ICosmosRepository<Cover> _cosmosRepository;

    public CoverService(ICosmosRepository<Cover> cosmosRepository)
    {
        _cosmosRepository = cosmosRepository;
    }
    
    public decimal ComputePremium(DateOnly startDate, DateOnly endDate, CoverType coverType)
    {
        var coverPremium = new Cover(startDate, endDate, coverType).Premium;
        return coverPremium;
    }

    public async Task<IEnumerable<Cover>> GetCoversAsync()
    { 
        return  await _cosmosRepository.GetItemsAsync();
    }

    public async Task<Cover?> GetCoverByIdAsync(string id)
    {
        return await _cosmosRepository.GetItemByIdAsync(id);
    }

    public async Task AddCoverAsync(Cover cover)
    {
        await _cosmosRepository.AddItemAsync(cover);
    }

    public async Task DeleteCoverAsync(string id)
    {
        await _cosmosRepository.DeleteItemAsync(id);
    }
}