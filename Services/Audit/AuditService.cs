using Data.Models;
using Services.AzureServiceBus;

namespace Services.Audit;

public class AuditService : IAuditService
{
    private readonly IAzureBusService _azureBusService;

    public AuditService(IAzureBusService azureBusService)
    {
        _azureBusService = azureBusService;
    }

    public async Task AuditClaim(string id, string httpRequestType)
    {
        var auditClaim = new ClaimAudit()
        {
            ClaimId = id,
            Created = DateTime.Now,
            HttpRequestType = httpRequestType
        };
        
        await _azureBusService.SendMessageAsync(auditClaim);
    }

    public async Task AuditCover(string id, string httpRequestType)
    {
        var auditCover = new CoverAudit()
        {
            CoverId = id,
            Created = DateTime.Now,
            HttpRequestType = httpRequestType
        };
        await _azureBusService.SendMessageAsync(auditCover);
    }
}