namespace Data.Models;

public class ClaimAudit : BaseAudit
{
    public string? ClaimId { get; set; }
}