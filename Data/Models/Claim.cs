using Data.Attributes;
using Data.Enums;
using Data.Interfaces;
using Newtonsoft.Json;

namespace Data.Models;

[GenericModelType]
public class Claim : IHasId
{
    public const decimal MaxDamageCostAllowed = 100000;
    
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; } = Guid.NewGuid().ToString();
    
    [JsonProperty(PropertyName = "coverId")]
    public string CoverId { get; set; }

    [JsonProperty(PropertyName = "created")]
    public DateTime Created { get; set; }

    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    [JsonProperty(PropertyName = "claimType")]
    public ClaimType Type { get; set; }

    [JsonProperty(PropertyName = "damageCost")]
    public decimal DamageCost { get; set; }
    
    public Claim(Cover cover, string name, decimal damageCost, ClaimType claimType, DateTime createdDate)
    {
        CoverId = cover.Id;
        Name = name;
        DamageCost = damageCost;
        Type = claimType;
        Created = createdDate;
    }
}