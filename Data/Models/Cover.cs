using Data.Attributes;
using Data.Enums;
using Data.Helpers;
using Data.Interfaces;
using Newtonsoft.Json;

namespace Data.Models;

[GenericModelType]
public class Cover : IHasId
{
    public const decimal BasePremiumDailyRate = 1250;
    
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; } = Guid.NewGuid().ToString();

    [JsonProperty(PropertyName = "startDate")]
    public DateOnly StartDate { get; set; }
    
    [JsonProperty(PropertyName = "endDate")]
    public DateOnly EndDate { get; set; }
    
    [JsonProperty(PropertyName = "claimType")]
    public CoverType Type { get; set; }

    [JsonProperty(PropertyName = "premium")]
    public decimal Premium { get; set; }

    public Cover() { }

    public Cover(DateOnly startDate,  DateOnly endDate, CoverType coverType)
    {
        StartDate = startDate;
        EndDate = endDate;
        Type = coverType;
        Premium = Calculator.ComputePremium(startDate, endDate, coverType);
    }
}