namespace Data.Models;

public class CoverAudit : BaseAudit
{
    public string? CoverId { get; set; }
}