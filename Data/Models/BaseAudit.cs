namespace Data.Models;

public abstract class BaseAudit
{
    public int Id { get; set; }
    public DateTime Created { get; set; }
    public string? HttpRequestType { get; set; }
}