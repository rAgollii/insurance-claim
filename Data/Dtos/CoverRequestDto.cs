using Data.Enums;

namespace Data.Dtos;

public class CoverRequestDto
{
    public DateOnly StartDate { get; set; }

    public DateOnly EndDate { get; set; }

    public CoverType CoverType { get; set; }
}