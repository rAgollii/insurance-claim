using Data.Enums;

namespace Data.Dtos;

public class ClaimRequestDto
{
    public string CoverId { get; set; } = null!;
    
    public DateTime Created { get; set; }
    
    public string Name { get; set; } = null!;

    public ClaimType Type { get; set; }

    public decimal DamageCost { get; set; }
}