using Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Data.Context;

public class AuditDbContext : DbContext
{
    public AuditDbContext(DbContextOptions<AuditDbContext> options) : base(options){ }
    
    public required DbSet<ClaimAudit> ClaimAudits { get; set; }
    public required DbSet<CoverAudit> CoverAudits { get; set; }
}