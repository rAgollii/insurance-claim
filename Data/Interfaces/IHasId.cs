namespace Data.Interfaces;

public interface IHasId
{
    string Id { get; }
}