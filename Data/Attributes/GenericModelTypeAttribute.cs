namespace Data.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class GenericModelTypeAttribute : Attribute { }