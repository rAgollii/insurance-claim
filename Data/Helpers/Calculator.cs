using Data.Enums;

namespace Data.Helpers;

public static class Calculator
{
    public static decimal BasePremiumDailyRate { get; private set; } = 1250;
    public static decimal ComputePremium(DateOnly startDate, DateOnly endDate, CoverType coverType)
    {
        decimal multiplier = GetMultiplierForCoverType(coverType);
        decimal premiumPerDay = BasePremiumDailyRate * multiplier;
        int insuranceLength = endDate.DayNumber - startDate.DayNumber;
        decimal totalPremium = 0m;

        for (int i = 0; i < insuranceLength; i++)
        {
            totalPremium += CalculatePremiumForDay(i, coverType, premiumPerDay);
        }

        return totalPremium;
    }

    public static decimal GetMultiplierForCoverType(CoverType coverType)
    {
        return coverType switch
        {
            CoverType.Yacht => 1.1m,
            CoverType.PassengerShip => 1.2m,
            CoverType.Tanker => 1.5m,
            _ => 1.3m,
        };
    }

    private static decimal CalculatePremiumForDay(int day, CoverType coverType, decimal premiumPerDay)
    {
        decimal discount = coverType == CoverType.Yacht ? 0.05m : 0.02m;
    
        if (day < 30)
        {
            return premiumPerDay;
        }

        if (day < 180)
        {
            return premiumPerDay - premiumPerDay * discount;
        }

        discount = coverType == CoverType.Yacht ? 0.08m : 0.03m;

        return premiumPerDay - premiumPerDay * discount;
    }
}