namespace Claims.Tests;

internal abstract class TestUtilities
{
    internal static DateOnly GetDateOnlyForCurrentOrFutureDate(double days = 0, int months = 0, int years = 0)
    {
        return DateOnly.FromDateTime(DateTime.UtcNow.AddDays(days).AddMonths(months).AddYears(years));
    }
    
    internal static DateTime GetDateTimeForCurrentOrFutureDate(double days = 0, int months = 0, int years = 0)
    {
        return  DateTime.UtcNow.AddDays(days).AddMonths(months).AddYears(years);
    }
    
}