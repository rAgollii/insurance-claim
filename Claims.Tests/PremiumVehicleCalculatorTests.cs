using Data.Enums;
using Data.Helpers;
using Data.Models;
using Moq;
using Xunit;

namespace Claims.Tests;

public class PremiumVehicleCalculatorTests
{
	[Fact]
	public void Yacht_should_be_10_percent_more_expensive_for_first_duration()
	{
		//Arrange
		var yachtPremium = Cover.BasePremiumDailyRate *  Calculator.GetMultiplierForCoverType(CoverType.Yacht);
		var expectedPremium = 10 * yachtPremium; 
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 10), CoverType.Yacht);
		//Nothing to Act
		
		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);
	}
	
	[Fact]
	public void PassengerShip_should_be_20_percent_more_expensive_for_first_duration()
	{
		//Arrange
		var passengerShipPremium = Cover.BasePremiumDailyRate *  Calculator.GetMultiplierForCoverType(CoverType.PassengerShip);
		var expectedPremium = 29 * passengerShipPremium;  
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 0), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 29), CoverType.PassengerShip);
		
		//Nothing to Act
		
		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);
	}
	
	[Fact]
	public void Tanker_should_be_50_percent_more_expensive_for_first_duration()
	{
		//Arrange
		var tankerPremium = Cover.BasePremiumDailyRate *  Calculator.GetMultiplierForCoverType(CoverType.Tanker);
		var expectedPremium = 20 * tankerPremium;  
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(),TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 20), CoverType.Tanker);
		//Nothing to Act
		
		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);

	}
	
	[Theory]
	[InlineData(CoverType.BulkCarrier)]
	[InlineData(CoverType.ContainerShip)]
	public void CoverType_should_be_30_percent_more_expensive_for_first_duration(CoverType type)
	{
		//Arrange
		var bulkCarrierAndContainerShipPremium = Cover.BasePremiumDailyRate * (type == CoverType.BulkCarrier ?  Calculator.GetMultiplierForCoverType(CoverType.BulkCarrier) :  Calculator.GetMultiplierForCoverType(CoverType.ContainerShip));
		var expectedPremium = 1 * bulkCarrierAndContainerShipPremium;
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 1), type);
		//Nothing to Act

		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);
	}

	[Fact]
	public void Yacht_should_be_discounted_by_5_the_next_150_days()
	{
		//Arrange
		var yachtPremium = Cover.BasePremiumDailyRate *  Calculator.GetMultiplierForCoverType(CoverType.Yacht);
		var premiumForDays30 = 30 * yachtPremium;
		var premiumForDays60 = 60 * (yachtPremium - yachtPremium * 0.05M);
		var expectedPremium = premiumForDays30 + premiumForDays60;
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 90), CoverType.Yacht);
		
		//Nothing to Act

		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);
	}
	
	[Fact]
	public void PassengerShip_should_be_discounted_by_2_the_next_150_days()
	{
		//Arrange
		var passengerShipPremium = Cover.BasePremiumDailyRate *  Calculator.GetMultiplierForCoverType(CoverType.PassengerShip);
		var premiumForDays30 = 30 * passengerShipPremium;
		var premiumFor100Days = 100 * (passengerShipPremium - passengerShipPremium * 0.02M);
		var expectedPremium = premiumForDays30 + premiumFor100Days;
		//Nothing to Act
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 130), CoverType.PassengerShip);

		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);
	}
	
	[Fact]
	public void Tanker_is_discounted_by_2_the_next_150_days()
	{
		//Arrange
		var tankerPremium = Cover.BasePremiumDailyRate *  Calculator.GetMultiplierForCoverType(CoverType.Tanker);
		var premiumForDays30 = 30 * tankerPremium;
		var premiumForDays90Days = 90 * (tankerPremium - tankerPremium * 0.02M);
		var expectedPremium = premiumForDays30 + premiumForDays90Days;
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 120), CoverType.Tanker);
		//Nothing to Act
		
		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);

	}
	
	[Theory]
	[InlineData(CoverType.BulkCarrier)]
	[InlineData(CoverType.ContainerShip)]
	public void Bulk_and_container_is_discounted_by_2_the_next_150_days(CoverType type)
	{
		//Arrange
		var bulkCarrierAndContainerShipPremium = Cover.BasePremiumDailyRate * (type == CoverType.BulkCarrier ?  Calculator.GetMultiplierForCoverType(CoverType.BulkCarrier) :  Calculator.GetMultiplierForCoverType(CoverType.ContainerShip));
		var premiumForDays30 = 30 * bulkCarrierAndContainerShipPremium;
		var premiumForDays60 = 60 * (bulkCarrierAndContainerShipPremium - bulkCarrierAndContainerShipPremium * 0.02M);
		var expectedPremium = premiumForDays30 + premiumForDays60;
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 90), type);
		//Nothing to Act

		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);
	}
	
	
		
	[Fact]
	public void Yacht_should_have_3_percent_more_discount_for_the_remaining_of_days()
	{
		//Arrange
		var yachtPremium = Cover.BasePremiumDailyRate *  Calculator.GetMultiplierForCoverType(CoverType.Yacht);
		var premiumForDays30 = 30 * yachtPremium;
		var premiumForDays150 = 150 * (yachtPremium - yachtPremium * 0.05M);
		var premiumForDays180 = 180 * (yachtPremium - yachtPremium * 0.08M);
		var expectedPremium = premiumForDays30 + premiumForDays150 + premiumForDays180;
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 360), CoverType.Yacht);
		
		//Nothing to Act

		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);
	}
	
	[Fact]
	public void PassengerShip_should_have_1_percent_more_discount_for_the_remaining_of_days()
	{
		//Arrange
		var passengerShipPremium = Cover.BasePremiumDailyRate *  Calculator.GetMultiplierForCoverType(CoverType.PassengerShip);
		var premiumFor30Days = 30 * passengerShipPremium;
		var premiumFor150Days = 150 * (passengerShipPremium - passengerShipPremium * 0.02M);
		var premiumFor180Days = 40 * (passengerShipPremium - passengerShipPremium * 0.03M);
		var expectedPremium = premiumFor30Days + premiumFor150Days + premiumFor180Days;
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 220), CoverType.PassengerShip);
		
		//Nothing to Act

		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);
	}
	
	
	[Fact]
	public void Tanker_should_have_1_percent_more_discount_for_the_remaining_of_days()
	{
		//Arrange
		var tankerPremium = Cover.BasePremiumDailyRate * Calculator.GetMultiplierForCoverType(CoverType.Tanker);
		var premiumFor30Days = 30 * tankerPremium;
		var premiumFor150Days = 150 * (tankerPremium - tankerPremium * 0.02M);
		var premiumFor20Days = 20 * (tankerPremium - tankerPremium * 0.03M);
		var expectedPremium = premiumFor30Days + premiumFor150Days + premiumFor20Days;
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 200), CoverType.Tanker);
		
		//Nothing to Act

		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);
	}
	
	
	[Theory]
	[InlineData(CoverType.BulkCarrier)]
	[InlineData(CoverType.ContainerShip)]
	public void Bulk_and_container_should_have_1_percent_more_discount_for_the_remaining_of_days(CoverType type)
	{
		//Arrange
		var bulkCarrierAndContainerShipPremium = Cover.BasePremiumDailyRate * (type == CoverType.BulkCarrier ?  Calculator.GetMultiplierForCoverType(CoverType.BulkCarrier) :  Calculator.GetMultiplierForCoverType(CoverType.ContainerShip));
		var premiumForDays30 = 30 * bulkCarrierAndContainerShipPremium;
		var premiumForDays60 = 150 * (bulkCarrierAndContainerShipPremium - bulkCarrierAndContainerShipPremium * 0.02M);
		var premiumFor20Days = 20 * (bulkCarrierAndContainerShipPremium * 1.3M - bulkCarrierAndContainerShipPremium * 0.03M);
		var expectedPremium = premiumForDays30 + premiumForDays60 + premiumFor20Days;
		var mockCover = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 0), TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 200), type);
		
		//Nothing to Act

		//Assert
		Assert.Equal(mockCover.Object.Premium, expectedPremium);
	}
}