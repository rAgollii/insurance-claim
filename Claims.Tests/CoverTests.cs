using Data.Enums;
using Data.Models;
using Moq;
using Xunit;

namespace Claims.Tests;

public class CoverTests
{
    
    [Fact]
    public void Start_date_should_not_be_in_the_past()
    {
        Assert.Throws<ArgumentException>(() =>
            new Mock<Cover>(
                TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: -1),
                TestUtilities.GetDateOnlyForCurrentOrFutureDate(days: 1),
                CoverType.Yacht));
    }
	
    [Fact]
    public void Insurance_period_should_not_pass_year()
    {
        Assert.Throws<ArgumentException>(() =>
            new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(), 
                TestUtilities.GetDateOnlyForCurrentOrFutureDate(days:1, years:1),
                CoverType.Yacht));
    }

    [Fact]
    public void Creating_Cover_should_be_successful()
    {
        DateOnly startDateParam = TestUtilities.GetDateOnlyForCurrentOrFutureDate();
        DateOnly endDateParam = TestUtilities.GetDateOnlyForCurrentOrFutureDate(years: 1);
        var coverTypeParam = CoverType.Yacht;
        
        var cover = new Mock<Cover>(startDateParam, endDateParam, coverTypeParam);

        Assert.NotNull(cover);
        Assert.Equal(cover.Object.StartDate, startDateParam);
        Assert.Equal(cover.Object.EndDate, endDateParam);
        Assert.Equal(cover.Object.Type, coverTypeParam);
    }
}