using Data.Enums;
using Data.Models;
using Moq;
using Xunit;

namespace Claims.Tests;

public class ClaimTests
{
    private readonly Mock<Cover> _coverMock;

    public ClaimTests()
    {
        _coverMock = new Mock<Cover>(TestUtilities.GetDateOnlyForCurrentOrFutureDate(),
            TestUtilities.GetDateOnlyForCurrentOrFutureDate(years: 1), 
            CoverType.Yacht);
    }
    
    
    [Theory]
    [InlineData(100001.01)]
    [InlineData(100001)]
    [InlineData(200000)]
    public void Damage_cost_should_throw_Exception_when_value_exceeds_100000(decimal damage)
    {
        Assert.Throws<ArgumentException>(() =>
            new Mock<Claim>(_coverMock, TestUtilities.GetDateTimeForCurrentOrFutureDate(days: 2, months: 2), "testClaimName", ClaimType.Fire, damage));
    }

    [Fact]
    public void Created_should_throw_Exception_when_Claim_date_in_past()
    {
        Assert.Throws<ArgumentException>(() =>
            new Mock<Claim>(_coverMock, TestUtilities.GetDateTimeForCurrentOrFutureDate(days: -6), "testClaimName", ClaimType.BadWeather, 1000));
    }
    
    [Fact]
    public void Create_claim_success()
    {
        var claimNameParam = "testClaimName";
        var claimDateParam = TestUtilities.GetDateTimeForCurrentOrFutureDate(months: 6);
        var claimTypeParam = ClaimType.Fire;
        var claimDamageCostParam = 1000;
        var claim = new Mock<Claim>(_coverMock, claimDateParam, claimNameParam, claimTypeParam, claimDamageCostParam);

        Assert.NotNull(claim);
        Assert.Equal(claim.Object.Name, claimNameParam);
        Assert.Equal(claim.Object.Created, claimDateParam);
        Assert.Equal(claim.Object.DamageCost, claimDamageCostParam);
        Assert.Equal(claim.Object.Type, claimTypeParam);
    }
}