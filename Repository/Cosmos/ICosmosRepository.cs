
namespace Repository.Cosmos;

public interface ICosmosRepository<T> where T : class
{
    Task<IEnumerable<T>> GetItemsAsync();
    Task<T?> GetItemByIdAsync(string id);
    Task AddItemAsync(T dataItem);
    Task DeleteItemAsync(string id);
}