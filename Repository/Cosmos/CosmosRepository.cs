using Data.Attributes;
using Data.Interfaces;
using Microsoft.Azure.Cosmos;

namespace Repository.Cosmos;

public class CosmosRepository<T>: ICosmosRepository<T> where T : class, IHasId
{
    private readonly Container _container;
    
    public CosmosRepository(CosmosClient dbClient,
        string databaseName,
        string containerName)
    {
        if (dbClient == null) throw new ArgumentNullException(nameof(dbClient));
        _container = dbClient.GetContainer(databaseName, containerName);
    }
    
    public async Task<IEnumerable<T>> GetItemsAsync()
    {
        if (Attribute.IsDefined(typeof(T), typeof(GenericModelTypeAttribute)))
        {
            var query = _container.GetItemQueryIterator<T>(new QueryDefinition("SELECT * FROM c"));
            var results = new List<T>();
            while (query.HasMoreResults)
            {
                var response = await query.ReadNextAsync();

                results.AddRange(response.ToList());
            }
            return results;
        }
        throw new InvalidOperationException("Invalid model type.");
    }

    public async Task<T?> GetItemByIdAsync(string id)
    {
        if (Attribute.IsDefined(typeof(T), typeof(GenericModelTypeAttribute)))
        {
            try
            {
                var response = await _container.ReadItemAsync<T>(id, new PartitionKey(id));
                return response.Resource;
            }
            catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return default(T);
            }
        }
        throw new InvalidOperationException("Invalid model type.");
    }

    public Task AddItemAsync(T dataItem)
    {
        if (Attribute.IsDefined(typeof(T), typeof(GenericModelTypeAttribute)))
        {
            return _container.CreateItemAsync(dataItem, new PartitionKey(dataItem.Id));
        }
        throw new InvalidOperationException("Invalid model type.");
    }

    public Task DeleteItemAsync(string id)
    {
        if (Attribute.IsDefined(typeof(T), typeof(GenericModelTypeAttribute)))
        { 
            return _container.DeleteItemAsync<T>(id, new PartitionKey(id));
        }
        throw new InvalidOperationException("Invalid model type.");
    }
}