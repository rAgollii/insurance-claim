using Claims.Logger;
using Data.Dtos;
using Data.Models;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Services.Audit;
using Services.Claim;
using Services.Cover;

namespace Claims.Controllers;

/// <summary>
/// Claims Controller that handles Claims endpoints processes
/// </summary>
[ApiController]
[Route("[controller]")]
public class ClaimsController : ControllerBase
{
    
    private readonly ILogger<ClaimsController> _logger;
    private readonly IValidator<ClaimRequestDto> _claimValidator;
    private readonly IClaimService _claimService;
    private readonly ICoverService _coverService;
    private readonly IAuditService _auditService;

    /// <summary>
    /// Controller constructor which has dependencies it relies on for the endpoints logic
    /// </summary>
    /// <param name="logger">Used to log the information</param>
    /// <param name="claimValidator">Dependency used for validating the request logic constrains</param>
    /// <param name="claimService">Dependency used for Claim logic</param>
    /// <param name="coverService">Dependency used for Cover logic</param>
    /// <param name="auditService">Dependency used for Audit logic</param>
    public ClaimsController(ILogger<ClaimsController> logger, IValidator<ClaimRequestDto> claimValidator, IClaimService claimService,
        ICoverService coverService, IAuditService auditService)
    {
        _logger = logger;
        _claimValidator = claimValidator;
        _claimService = claimService;
        _coverService = coverService;
        _auditService = auditService;
    }

    /// <summary>
    /// Gets all the list of Claims
    /// </summary>
    /// <returns>Claims requested or nothing</returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<Claim>>> GetAsync()
    {
        var claims = await _claimService.GetClaimsAsync();
        return Ok(claims);
    }

    /// <summary>
    /// Gets Claim by Id
    /// </summary>
    /// <param name="id">Id of claim requested</param>
    /// <returns>Claim of id requested or Not found</returns>
    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Claim>> GetAsync(string id)
    {
        var claim = await _claimService.GetClaimByIdAsync(id);
        if (claim != null)
        {
            return Ok(claim);
        }
        _logger.LogWarning(AppLogEvents.ReadNotFound, "GetAsync({0}) Claim not found. Logged on {1:MMMM dd, yyyy}", id, DateTime.UtcNow);
        return NotFound();
    }
    
    /// <summary>
    /// Creates a new Claim and Audits it
    /// </summary>
    /// <param name="claimRequestDto">Claim request model to create Claim</param>
    /// <returns>Status 200 with the Claim or Bad Request</returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> CreateAsync(ClaimRequestDto claimRequestDto)
    {
        var validationResult = await _claimValidator.ValidateAsync(claimRequestDto);
        if (!validationResult.IsValid) {
            return BadRequest(validationResult.Errors.ToArray());
        }
        
        var cover = await _coverService.GetCoverByIdAsync(claimRequestDto.CoverId);
        var claim = new Claim(cover!, claimRequestDto.Name, claimRequestDto.DamageCost, claimRequestDto.Type,
            claimRequestDto.Created);
        await _claimService.AddClaimAsync(claim);
        await _auditService.AuditClaim(claim.Id, "POST");
        return Ok(claim);
    }

    /// <summary>
    /// Delete Claim by Id and Audit it
    /// </summary>
    /// <param name="id">Claim of id requested</param>
    /// <returns>Accepted status to indicate that the request was successful</returns>
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status202Accepted)]
    public async Task<ActionResult> DeleteAsync(string id)
    {
        await _claimService.DeleteClaimAsync(id);
        await _auditService.AuditClaim(id, "DELETE");
        return Accepted();
    }
}
