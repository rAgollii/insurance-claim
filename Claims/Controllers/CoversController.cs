using Claims.Logger;
using Data.Dtos;
using Data.Models;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Services.Audit;
using Services.Cover;

namespace Claims.Controllers;

/// <summary>
/// Covers Controller that handles Covers endpoints processes
/// </summary>
[ApiController]
[Route("[controller]")]
public class CoversController : ControllerBase
{
    private readonly ILogger<CoversController> _logger;
    private readonly IValidator<CoverRequestDto> _validator;
    private readonly ICoverService _coverService;
    private readonly IAuditService _auditService;

    /// <summary>
    /// Controller constructor which has dependencies it relies on for the endpoints logic
    /// </summary>
    /// <param name="logger">Used to log the information</param>
    /// <param name="validator">Dependency used for validating the request logic constrains</param>
    /// <param name="coverService">Dependency used for Cover logic</param>
    /// <param name="auditService">Dependency used for Audit logic</param>
    public CoversController(ILogger<CoversController> logger, IValidator<CoverRequestDto> validator, ICoverService coverService, IAuditService auditService)
    {
        _logger = logger;
        _validator = validator;
        _coverService = coverService;
        _auditService = auditService;
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="coverRequestDto"></param>
    /// <returns></returns>
    [HttpPost("premium")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> ComputePremiumAsync(CoverRequestDto coverRequestDto)
    {
        var validationResult = await _validator.ValidateAsync(coverRequestDto);
        if (!validationResult.IsValid) {
            return BadRequest(validationResult.Errors.ToArray());
        }
        _logger.Log(LogLevel.Information, AppLogEvents.Details, "Starting to compute Premium");
        var coverPremium = _coverService.ComputePremium(coverRequestDto.StartDate, coverRequestDto.EndDate, coverRequestDto.CoverType);
        _logger.Log(LogLevel.Information, AppLogEvents.Details, "Premium computation successful");
        return Ok(coverPremium); 
    }

    /// <summary>
    /// Gets all the list of Covers
    /// </summary>
    /// <returns>Covers requested or nothing</returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<Cover>>> GetAsync()
    {
        var results = await _coverService.GetCoversAsync();
        return Ok(results);
    }

    /// <summary>
    /// Gets Cover by Id
    /// </summary>
    /// <param name="id">Id of cover requested</param>
    /// <returns>Claim of id requested or Not found</returns>
    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Cover>> GetAsync(string id)
    {
        var result = await _coverService.GetCoverByIdAsync(id);
        if (result != null) {
            return Ok(result);
        }
        _logger.LogWarning(AppLogEvents.ReadNotFound, "GetAsync({0}) Cover not found. Logged on {1:MMMM dd, yyyy}", id, DateTime.UtcNow);
        return NotFound();
    }

    /// <summary>
    /// Creates a new Cover and Audits it
    /// </summary>
    /// <param name="coverRequestDto">Cover request model to create Cover</param>
    /// <returns>Status 200 with the Cover or Bad Request</returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> CreateAsync(CoverRequestDto coverRequestDto)
    {
        var validationResult = await _validator.ValidateAsync(coverRequestDto);
        if (!validationResult.IsValid) {
            return BadRequest(validationResult.Errors.ToArray());
        }
        
        var cover = new Cover(coverRequestDto.StartDate, coverRequestDto.EndDate, coverRequestDto.CoverType);
        await _coverService.AddCoverAsync(cover);
        await _auditService.AuditCover(cover.Id, "POST");
        return Ok(cover);
    }

    /// <summary>
    /// Delete Cover by Id and Audits it
    /// </summary>
    /// <param name="id">Cover of id requested</param>
    /// <returns>Accepted status to indicate that the request was successful</returns>
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status202Accepted)]
    public async Task<ActionResult> DeleteAsync(string id)
    {
        await _coverService.DeleteCoverAsync(id);
        await _auditService.AuditCover(id, "DELETE");
        return Accepted();
    }
}