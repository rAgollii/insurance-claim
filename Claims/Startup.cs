using System.Text.Json.Serialization;
using Claims.Settings;
using Claims.Validators;
using Data.Context;
using Data.Dtos;
using Data.Interfaces;
using Data.Models;
using FluentValidation;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Repository.Cosmos;
using Serilog;
using Services.Audit;
using Services.AzureServiceBus;
using Services.Claim;
using Services.Cover;
using IApplicationBuilder = Microsoft.AspNetCore.Builder.IApplicationBuilder;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using IServiceCollection = Microsoft.Extensions.DependencyInjection.IServiceCollection;
using IWebHostEnvironment = Microsoft.AspNetCore.Hosting.IWebHostEnvironment;
using OpenApiInfo = Microsoft.OpenApi.Models.OpenApiInfo;

namespace Claims;
public class Startup
{
    private readonly IConfiguration Configuration;
    private const string AllowedOrigins = "allOrigins";
    
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo {Title = "Insurance Claims", Version = "v1"});
            c.DocInclusionPredicate((docName, description) => true);
        });
        
        services.AddDbContext<AuditDbContext>(options =>
        {
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
        }, ServiceLifetime.Singleton);
        var cosmosDbConfig = Configuration.GetSection("CosmosDb");
        var cosmosDbName = cosmosDbConfig.GetValue<string>("DatabaseName");
        var cosmosContainerName = cosmosDbConfig.GetValue<string>("ContainerName");
        var cosmoAccount = cosmosDbConfig.GetValue<string>("Account");
        var cosmoKey = cosmosDbConfig.GetValue<string>("Key");
        services.AddScoped<IClaimService, ClaimService>((sp) =>
        {
            var cosmosClient = CosmosClientInstanceAsync<Claim>(cosmosDbName, cosmosContainerName, cosmoAccount, cosmoKey).GetAwaiter().GetResult();
            return new ClaimService(cosmosClient);
        });

        services.AddScoped<ICoverService>((sp) =>
        {
            var cosmosClient = CosmosClientInstanceAsync<Cover>(cosmosDbName, "Cover", cosmoAccount, cosmoKey).GetAwaiter().GetResult();
            return new CoverService(cosmosClient);
        });

        services.AddScoped<IAuditService, AuditService>();
        services.AddTransient<IValidator<ClaimRequestDto>, ClaimValidator>();
        services.AddTransient<IValidator<CoverRequestDto>, CoverValidator>();
        services.AddSingleton<IAzureBusService, AzureBusService>(provider =>
        {
            var azureServiceBusConfig = Configuration.GetSection("AzureServiceBus");
            var connectionString = azureServiceBusConfig.GetValue<string>("ConnectionString");
            var queueName = azureServiceBusConfig.GetValue<string>("QueueName");

            return new AzureBusService(connectionString ?? null, queueName ?? null);
        });
        
        services.AddControllers().AddJsonOptions(x =>
            {
                x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            }
        );
        
        services.AddCors(options =>
        {
            options.AddPolicy(AllowedOrigins, builder =>
            {
                builder.WithOrigins("*");
                builder.WithHeaders("*");
                builder.WithMethods("*");
            });
        });
        services.AddLogging(loggingBuilder =>
        {
            loggingBuilder.AddSerilog();
        });
        services.AddHttpContextAccessor();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
    }

    private static async Task<ICosmosRepository<T>> CosmosClientInstanceAsync<T>(string dbName, string containerName, string account, string key) where T: class, IHasId
    {
        var client = new Microsoft.Azure.Cosmos.CosmosClient(account, key);
        var cosmosDbService = new CosmosRepository<T>(client, dbName, containerName);
        var database = await client.CreateDatabaseIfNotExistsAsync(dbName);
        await database.Database.CreateContainerIfNotExistsAsync(containerName, "/id");

        return cosmosDbService;
    }
    
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        // Configure the HTTP request pipeline.
        if (env.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        
        app.UseHttpsRedirection();

        app.UseRouting();

        app.UseCors(AllowedOrigins);
        app.UseAuthentication();
        app.UseAuthorization();

        app.UseSwagger();
        app.UseSwaggerUI(s => s.SwaggerEndpoint("/swagger/v1/swagger.json", "Insurance Claims Api v.1"));

        var option = new RewriteOptions();
        option.AddRedirect("^$", "swagger");
        app.UseRewriter(option);
        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
}