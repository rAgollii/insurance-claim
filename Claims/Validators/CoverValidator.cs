using Data.Dtos;
using FluentValidation;

namespace Claims.Validators;

internal class CoverValidator: AbstractValidator<CoverRequestDto>
{
    public CoverValidator()
    {
        RuleFor(c => c.StartDate)
            .Must(StartDateIsNotInPast)
            .WithMessage("StartDate cannot be in the past.");

        RuleFor(c => c)
            .Must(c =>  PeriodDoesntExceedYear(c.StartDate, c.EndDate))
            .WithMessage("Total insurance period cannot exceed 1 year");
    }
    
    private static bool StartDateIsNotInPast(DateOnly startDate)
    {
        var dateNowUtc = DateOnly.FromDateTime(DateTime.UtcNow.Date);
        return startDate >= dateNowUtc;
    }

    private static bool PeriodDoesntExceedYear(DateOnly startDate, DateOnly endDate)
    {
        int totalDays = endDate.DayNumber - startDate.DayNumber;
        return totalDays <= 365;
    }
}