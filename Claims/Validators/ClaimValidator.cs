using Data.Dtos;
using Data.Enums;
using Data.Models;
using FluentValidation;
using Services.Cover;

namespace Claims.Validators;

internal class ClaimValidator: AbstractValidator<ClaimRequestDto>
{
    private readonly ICoverService _coverService;
    
    public ClaimValidator(ICoverService coverService)
    {
        _coverService = coverService;
        
        RuleFor(c => c.DamageCost)
            .GreaterThanOrEqualTo(Claim.MaxDamageCostAllowed)
            .WithMessage("DamageCost cannot exceed 100.000");

        RuleFor(c => c)
            .MustAsync(async (c, _) =>  await _coverService.GetCoverByIdAsync(c.CoverId) == null)
            .WithMessage("Cover does not exists")
            .MustAsync(async (c, _) => await CreatedDateIsInCoverPeriod(c.CoverId, c.Created))
            .WithMessage("Created Date must be in Cover Period");

        RuleFor(c => c.Name)
            .NotEmpty().WithMessage("Name cannot be empty")
            .MaximumLength(100).WithMessage("Name cannot exceed 100 characters");
        
        RuleFor(c => c.Created)
            .GreaterThanOrEqualTo(DateTime.UtcNow)
            .WithMessage("Created Date cannot be in the future");

        RuleFor(c => c.Type)
            .Must(value => !Enum.IsDefined(typeof(ClaimType), value)).WithMessage("Invalid ClaimType");
    }
    
    private async Task<bool> CreatedDateIsInCoverPeriod(string coverId, DateTime claimCreatedDate)
    {
        var cover = await _coverService.GetCoverByIdAsync(coverId);

        if (cover == null) throw new InvalidOperationException("Cover doesn't exist!");
        
        var startDate = cover.StartDate.ToDateTime(TimeOnly.MinValue);
        var endDate = cover.EndDate.ToDateTime(TimeOnly.MaxValue);
        return endDate > claimCreatedDate &&
               startDate < claimCreatedDate;
    }
}